import {calculateBonuses} from "./bonus-system.js";


describe('Test bonus-system', () => {
    console.log("Tests started");

    test('amount < 10000', (done) => {
        expect(calculateBonuses("Null", -199999)).toEqual(0);
        done();
    });

    test('amount < 10000', (done) => {
        expect(calculateBonuses("Null", 1)).toEqual(0);
        done();
    });

    test('amount < 50000', (done) => {
        expect(calculateBonuses("Null", 40000)).toEqual(0);
        done();
    });

    test('amount < 100000', (done) => {
        expect(calculateBonuses("Null", 60000)).toEqual(0);
        done();
    });

    test('amount >= 100000', (done) => {
        expect(calculateBonuses("Null", 100000)).toEqual(0);
        done();
    });

    test('amount < 10000', (done) => {
        expect(calculateBonuses("Standard", 1)).toEqual(0.05);
        done();
    });

    test('amount < 50000', (done) => {
        expect(calculateBonuses("Standard", 40000)).toEqual(0.05 * 1.5);
        done();
    });

    test('amount < 100000', (done) => {
        expect(calculateBonuses("Standard", 60000)).toEqual(0.05 * 2);
        done();
    });

    test('amount >= 100000', (done) => {
        expect(calculateBonuses("Standard", 100000)).toEqual(0.05 * 2.5);
        done();
    });

    test('amount < 10000', (done) => {
        expect(calculateBonuses("Premium", 1)).toEqual(0.1);
        done();
    });

    test('amount < 50000', (done) => {
        expect(calculateBonuses("Premium", 40000)).toEqual(0.1 * 1.5);
        done();
    });

    test('amount < 100000', (done) => {
        expect(calculateBonuses("Premium", 60000)).toEqual(0.1 * 2);
        done();
    });

    test('amount >= 100000', (done) => {
        expect(calculateBonuses("Premium", 100000)).toEqual(0.1 * 2.5);
        done();
    });

    test('amount < 10000', (done) => {
        expect(calculateBonuses("Diamond", 1)).toEqual(0.2);
        done();
    });

    test('amount < 50000', (done) => {
        expect(calculateBonuses("Diamond", 40000)).toEqual(0.2 * 1.5);
        done();
    });

    test('amount < 100000', (done) => {
        expect(calculateBonuses("Diamond", 60000)).toEqual(0.2 * 2);
        done();
    });

    test('amount >= 100000', (done) => {
        expect(calculateBonuses("Diamond", 100000)).toEqual(0.2 * 2.5);
        done();
    });

    test('amount < 10000', (done) => {
        expect(calculateBonuses("Diamond", 1)).toEqual(0.2);
        done();
    });

    test('amount < 50000', (done) => {
        expect(calculateBonuses("Diamond", 40000)).toEqual(0.2 * 1.5);
        done();
    });

    test('amount < 100000', (done) => {
        expect(calculateBonuses("Diamond", 60000)).toEqual(0.2 * 2);
        done();
    });

    test('amount >= 100000', (done) => {
        expect(calculateBonuses("Diamond", 100000)).toEqual(0.2 * 2.5);
        done();
    });

    console.log('Tests Finished');

});




